FROM pgrm/grav:1.3.4

MAINTAINER Lleialtec

RUN rm -rf /usr/src/grav/user/pages/
ADD ./pages /usr/src/grav/user/pages
