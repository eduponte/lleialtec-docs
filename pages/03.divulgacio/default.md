---
title: Divulgació
hide_git_sync_repo_link: false
---

## Divulgació

Durant les portes obertes de la Lleialtat vam dinamitzar dues xerrades, una sobre el propi model tecnològic de la Lleialtat i l'altra, sobre les connexions entre el la comunitat del programari lliure i les iniciatives de l'Economia Social i Solidària (ESS). Les dues xerrades van tenir força participació i se'ns va demanar si tindrien continuació, amb el que detectem que hi ha tanta curiositat com manca d'informació en temes de tecnologies lliures, ètiques i distribuïdes. Actualment, s'han proposat tres xerrades més de cara octubre, novembre i desembre per debatre sobre ESS, moneda social i Blockchain.
