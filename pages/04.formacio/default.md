---
title: Formació
hide_git_sync_repo_link: false
---

## Formació

La manca de consciencia digital juga en contra de la sobirania tecnològica de la ciutadania i, per tant, de la ciutat.
I no hi ha encara cap pla municipal de barri que tingui la intenció d' informar i formar a la ciutadania en relació a les tecnologies existents.

Durant les portes obertes es va fer un taller de creació de videojocs que va tenir molt bona rebuda. Com se'ns va demanar que el tornéssim a fer, hem proposat cursets i tallers amb aquesta temàtica de cara la programació d'octubre a desembre.

A més, es proposen altres tallers i cursos formatius sobre seguretat informàtica bàsica i hol·lística, sistemes operatius lliures, i creació i maquetació de continguts. Alguns, amb perspectiva de gènere.
Per fer acció comunitària internacional i visibilitzar problemàtiques tecnològiques actuals, a l'última trobada de la CELS, es va consensuar donar suport a la campanya de la Free Software Foundation «[Public money, public code](https://publiccode.eu/ca/)» que demana a les Institucions alliberar el codi que es crea amb diners públics.


Tasques: 

* Formació en conceptes bàsics: com funciona Internet? Què és “el núvol”? Què són i per què són importants la neutralitat i la privacitat a la Xarxa?
* Concienciació sobre el disseny ètic de la tecnologia (Ethical Design)
* Alfabetització digital i formació en nous oficis: creador/a de continguts digitals, formador/a en tecnologies creatives, coordinador/a de projectes tecnològics, etc.
* Infomació i formació sobre disrupcions tecnològiques: Internet Of Things (IOT), Blockchain en forma de criptomonedes o contractes intel·ligents, etc.
