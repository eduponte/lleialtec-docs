---
title: Inici
hide_git_sync_repo_link: false
---

## Pla de Sobirania Tecnològica als equipaments municipals

## Presentació
«Tecnologia de les cures»

El Lleialtec és la comissió tecnològica de la Coordinadora d'Entitats per la Lleialtat Santsenca (CELS). I neix amb l'objectiu de vetllar per l'apoderament digital de les persones i col·lectius del barri. Per fer-ho, proposem a l'assemblea de la CELS eines tecnològiques lliures i ètiques per tal de gestionar l'equipament, comunicar-nos, crear contingut, emmagatzemar dades, etc.

Ens adonem que el Lleialtec és la comissió més transversal de la CELS, ja que totes les comissions, -en major o menor mesura-, tenen necessitats tecnològiques. A més, detectem que tots els equipaments tenen necessitats semblants: publicació de les d'activitats, inscripcions, reserva de sales i conseqüent facturació, comunicació interna i externa, entre d'altres.

Tot i així, observem que els perfils professionals dels equipaments, no contemplen cap figura que gestioni, -de forma integral-, les necessitats tecnològiques. Per començar donant exemple -i per protegir la ciutadania dels pesticides tecnològics-, creiem fermament que tots els equips municipals haurien de funcionar amb sistemes operatius i aplicacions lliures, i que, als equipaments, hi hauria d'haver personal especialitzat i formació en aquests temes.

Però, en general, al no haver-hi consciència digital, contínuament es fan servir eines privatives pel simple fet de ser gratuïtes o populars. I «si és gratis, el producte ets tu...». És per això, que des del Lleialtec, proposem un pla replicable de Sobirania Tecnològica enfocada a la ciutadania que es duria a terme a través dels equipaments municipals. A la jungla digital, protegir-se és protegir. I veiem en la higiene digital i en la lluita contra el capitalisme cognitiu, pràctiques del segle XXI que podríem anomenar «Tecnologia de les cures».

## Coherència i sostenibilitat

La Lleialtat és un equipament nou i en aquest sentit creiem que pot ser el banc de proves ideal per desenvolupar un pla que pugui ser replicable a d’altres equipaments o xarxes ciutadanes, que per les seves innèrcies quotidianes, tindrien dificultats per implementar canvis sense tenir un model de referència.

Creiem que per tal que el pla sigui sostenible, el Lleialtec necessita suport econòmic i polític. No parlem d'implementar eines i esperar a que la ciutadania les utilitzi: parlem de conscienciar divulgant, formant, acompanyant... Han de ser les pròpies persones les que, des de la seva pròpia consciència i coherència, valorin la importància de fer ús d'eines ètiques i distribuïdes per tal d'esdevenir una societat digital realment lliure.

Així com es fa amb l'ESS o la perspectiva de gènere, pensem que, en aquest inici del segle XXI, és vital destinar recursos per a conscienciar la ciutadania sobre tecnologies lliures i ètiques.

## Línies i propostes
Com a prova pilot del pla, proposem continuar l'activitat a l'equipament de la Lleialtat Santsenca, on les línies d'acció són tres: [infraestructura](https://tec.lleialtat.cat/infraestructura), per ser propietàries de les nostres dades i xarxes; [divulgació](https://tec.lleialtat.cat/divulgacio), per fer comprensible a la ciutadania la importància, al segle XXI, de les tecnologies lliures, ètiques i distribuïdes; i [formació](https://tec.lleialtat.cat/formacio), per tal fomentar l'alfabetització digital i l'aprenentatge al llarg de la vida.