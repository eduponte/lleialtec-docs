---
title: Infraestructura
hide_git_sync_repo_link: false
---

## Infraestructura

Crear un entorn on, un ampli conjunt del veïnat, tingui coneixements sobre les instal·lacions de l'equipament, sent capaços de mantenir les instal·lacions i replicar a altres equipaments. Les eines, la infraestructura i les tasques que falten per fer es realitzaran sempre acompanyades de documentació i tallers de formació per assegurar que les instal·lacions TIC de La Lleialtat són conegudes per persones del barri i per persones d'altres equipaments municipals, fent així replicable i sostenible el projecte.

Tasques: 

* Consensuar la tecnologia que s'implementi a l'edifici amb un grup més ampli del veïnat (LleialTec) que s'estigui involucrant en les instal·lacions de La Lleialtat 
* Organitzar els tallers corresponents per a la formació en les competències tècniques que calguin
* Formar grups de treball per a realitzar i implentar tasques
* Donar suport a l'equip tècnic de l'equipament i a les persones associades a la CELS

### Infraestructura ja realitzada

* Núvol per a emmagatzemar i compartir arxius (NextCloud)
* Âgora per a la comunicació interna (Discourse)
* Gestor de continguts web (Wordpress-Grav)
* Sistemes operatius lliures als ordinadors de l'equipament (Ubuntu Mate)
* Abastiment de wifi (cortesía de Guifinet)

### En desenvolupament

* Creació d'un software de gestió integral de l'equipament (tipus ERP) que ens permeti gestionar socis, reservar sales, facturar activitats, fer seguiment d'inventari, etc.
* Instal·lació i configuració del tallafocs
* Implementació de la infraestructura de Xarxa Guifinet
* Implementació d'un sistema de còpies de seguretat
* Configuració de llocs de treball en relació l'autentificació de les persones en correu i documents
* Adquisició, instal·lació i configuració d'un servidor (virtualització) per allotjar serveis com:

   * El programari per gestionar l'edifici (sales, calendari, base de dades dels socis, facturació, ingressos, despeses, etc. - per definir)	
   * Carpetes compartides 
   * Autentificació d'usuaris
