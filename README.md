# Lleialtec Docs

Continguts del site de la [Lleialtec](https://tec.lleialtat.cat/)

Construït sobre [Grav](https://getgrav.org/), un CMS en fitxers plans.

## Fluxe principal dels continguts

Els continguts de Lleialtec es sincronitzen bidireccionalment mitjançant el plugin [Git Sync](https://github.com/trilbymedia/grav-plugin-git-sync) de Grav.

## Entorn local

En determinats casos pot ser interessant disposar d'un entorn local, p.e. per evaluar noves taxonomies, o canvis en els _plugins_ i _themes_.

### Prerequisits

1. [Docker](https://docs.docker.com/engine/installation/)
2. [Docker Compose](https://docs.docker.com/compose/install/#install-compose)

### Setup

```bash
docker-compose up -d
```
Si tot va bé, s'executa un contenidor de docker amb grav i les pàgines de `/pages` afegides al mateix, escoltant al port 8080:

```bash
$ docker ps
CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS              PORTS                    NAMES
6cf7649edb5a        lleialtecdocs_site   "/bin/sh /entrypoi..."   17 minutes ago      Up 17 minutes       0.0.0.0:8080->80/tcp     lleialtec-docs
```
Un cop iniciat el contenidor ja és possible accedir a l'[admin de Grav](http://localhost:8080/admin) amb una rèplica carregada dels continguts de [Lleialtec](http://localhost:8080/inici).
